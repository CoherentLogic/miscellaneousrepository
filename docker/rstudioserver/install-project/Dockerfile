#
# WARNING: **YOU** need to fix the script below (see LICENSE x 2, below) in order to run this script to completion
#          since the Oracle license must be agreed to by **YOU** so READ THE LICENSE and then FIX THE CODE IF
#          **YOU** AGREE TO IT.
#
# -----
#
# This script:
#
# 1.) Is based on the Rocker RStudio Server image
# 2.) Removes OpenJDK
# 3.) Installs the Oracle JDK 11
# 4.) Checks out the rGroovy package from Git (for testing that this works).
# 5.) Installs Groovy
# 6.) Initializes packrat on the rGroovy package directory.
#
# ----- Testing begins -----
#
# docker stop $(docker ps -aq)
# docker rm $(docker ps -aq)
# docker rmi $(docker images -q)
# docker build - <Dockerfile
# echo NOW: docker run -d -p 8787:8787 -e PASSWORD=S0meP@sswerd-123456 --name clrstudio -i -t [id]
#
# ----- Testing Ends / Loading rGroovy Begins -----
#
# groovyJars <- list ("/usr/share/groovy/lib/groovy-2.4.8.jar")
# options(GROOVY_JARS=groovyJars)
# devtools::test()
#

FROM rocker/rstudio:latest

#
# docker exec -it clrstudio /bin/bash
#
# SEE: https://www.linuxuprising.com/2018/10/how-to-install-oracle-java-11-in-ubuntu.html
#
RUN su -
RUN echo "deb http://ppa.launchpad.net/linuxuprising/java/ubuntu bionic main" | tee /etc/apt/sources.list.d/linuxuprising-java.list
RUN apt-get update
RUN apt-get -y install apt-utils
RUN apt-get -y install gnupg2
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 73C3DB2A
RUN apt-get update
#
# SEE: https://www.jamescoyle.net/how-to/2417-install-oracle-java-in-debian-ubuntu-using-apt-get
#      https://www.linuxuprising.com/2018/10/how-to-install-oracle-java-11-in-ubuntu.html
#      https://github.com/docker-library/openjdk
#
# TODO: Disable auto-agree below before publishing.
#

RUN apt-get -y purge openjdk-8-jre openjdk-8-jre-headless

#
# Required when installing devtools -- see here:
#
# https://stackoverflow.com/questions/20923209/problems-installing-the-devtools-package
#
RUN apt-get install -y libcurl4-gnutls-dev

#
# Installing libomp-dev liblzma-dev r-cran-rjava fixes "recipe for target 'jri' failed".
#
# See here:
#
# https://github.com/s-u/rJava/issues/161
# https://github.com/rocker-org/rocker-versioned/issues/11
#

#
# LICENSE: Below won't work unless **YOU** agree to the Oracle license terms so **YOU** will need to READ THE LICENSE and then
#          fix the script accordingly.
#
DISABLED RUN echo oracle-java11-installer shared/accepted-oracle-license-v1-2 select true | sudo /usr/bin/debconf-set-selections

#
# LICENSE: Below won't work unless **YOU** agree to the Oracle license terms so **YOU** will need to READ THE LICENSE and then
#          fix the script accordingly.
#
RUN apt-get install [-y --yes] libomp-dev liblzma-dev r-cran-rjava oracle-java11-installer
RUN apt-get install [-y --yes] oracle-java11-set-default

RUN R CMD javareconf

RUN chown rstudio:rstudio /usr/local/lib/R/
RUN chown -R rstudio:rstudio /usr/local/lib/R/

RUN apt-get install -y groovy

#
RUN git clone https://bitbucket.org/CoherentLogic/rgroovy.git /usr/local/share/rGroovy
RUN chown -R rstudio:rstudio /usr/local/share/rGroovy/
RUN chmod +rwx /usr/local/share/rGroovy/
RUN chmod +rwx /usr/local/share/rGroovy/*
#
# See: https://rstudio.github.io/packrat/
#
RUN R -e 'install.packages("devtools")'
RUN R -e 'install.packages("packrat")'
#RUN R -e 'install.packages("rJava")'
#RUN R -e 'install.packages(c("RJSONIO", DEPENDENCIES=TRUE))'

#
#
RUN R -e 'packrat::init("/usr/local/share/rGroovy/")'

# Not sure if this is necessary yet however note that it can take a long time -- see below:
#
# https://github.com/rstudio/packrat/issues/347
#
# RUN R -e 'packrat::snapshot()'

# This is optional as the user may not be using local repos.
# RUN R -e 'packrat::set_opts(local.repos = "<path_to_repo>")'

# RUN R -e 'packrat::status()'

RUN apt-get update
